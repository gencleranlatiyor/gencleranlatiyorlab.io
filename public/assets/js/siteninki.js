
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-35687006-1']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();



function labelMaker(lang)
{
    var renkler=['zaman', 'iliskiler', 'siyaset', 'mekanlar','karsilasmalar', 'egitim', 'ekonomi', 'oznellik'];
    var rnd= Math.floor(Math.random()*8);

    $('#baslik').addClass(renkler[rnd]);

    if( lang == 'tr' ) {

        $('a.egitim').attr("title", "EĞİTİM");

        $('a.iliskiler').attr("title", "İLİŞKİLER");

        $('a.siyaset').attr("title", "SİYASET");

        $('a.oznellik').attr("title", "ÖZNELLİK");

        $('a.zaman').attr("title", "ZAMAN");

        $('a.ekonomi').attr("title", "EKONOMİLER");

        $('a.karsilasmalar').attr("title", "KARŞILAŞMALAR");
        
        $('a.mekanlar').attr("title", "MEKAN");
    }
    if( lang == 'en' ) {

        $('a.egitim').attr("title", "EDUCATION");

        $('a.iliskiler').attr("title", "RELATIONS");

        $('a.siyaset').attr("title", "POLITICS");

        $('a.oznellik').attr("title", "SUBJECTIVITIES");

        $('a.zaman').attr("title", "TIME");

        $('a.ekonomi').attr("title", "ECONOMIES");

        $('a.karsilasmalar').attr("title", "ENCOUNTERS");
        
        $('a.mekanlar').attr("title", "PLACES");
    }
    if( lang == 'ku' ) {

        $('a.egitim').attr("title", "PERWERDE");

        $('a.iliskiler').attr("title", "TEKİLİ");

        $('a.siyaset').attr("title", "SİYASET");

        $('a.oznellik').attr("title", "KİRDEWARİ");

        $('a.zaman').attr("title", "DEM");

        $('a.ekonomi').attr("title", "ABORİ");

        $('a.karsilasmalar').attr("title", "RASTÎHEVHATIN");
        
        $('a.mekanlar').attr("title", "CIH");
    }


    $('.siyaset').tooltip({});
    $('.iliskiler').tooltip({});
    $('.oznellik').tooltip({});
    $('.zaman').tooltip({});
    $('.ekonomi').tooltip({});
    $('.karsilasmalar').tooltip({});
    $('.egitim').tooltip({});
    $('.mekanlar').tooltip({});
}


var urlRoot = '/assets/img/al/';
var lang_prefix = '/';
var currentLang = 'tr';

function imageUrlBuilder(name)
{   
    return urlRoot+name+'.png';
}

//Gösterilmeyecek resimler
var filesToOmit = [];
var filesToChangeEng = [];

//Diyarbakır
filesToOmit.push('ahmet_kak_egitim_siya_ozne_diyarbakir2');
filesToOmit.push('asim_kak_egitim_siya_kars_diyarbakir8');
filesToOmit.push('asim_ua_kars_eko_mek_diyarbakir10');
filesToOmit.push('asim_uak_kars_eko_mek_diyarbakir11');
filesToOmit.push('elif_kak_zaman_mekan_siya_diyarbakir20');
filesToOmit.push('elif_uak_siyas_zam_mek_diyarbakir23');
filesToOmit.push('gul_kak_iliskiler_zam_siya_diyarbakir56');
filesToOmit.push('gul_uak_ilisk_siy_kars_diyarbakir59');
filesToOmit.push('lutfu_kak_oznellik_siya_kar_diyarbakir26');
filesToOmit.push('lutfu_uak_ilisk_ozn_kars_diyarbakir29');
filesToOmit.push('mehmet_kak_ekonomiler_diyarbakir32');
filesToOmit.push('mehmet_uak_kars_ozn_mek_diyarbakir35');
filesToOmit.push('remzi_kak_oznellik_egit_zama_diyarbakir38');
filesToOmit.push('remzi_uak_zam_ozn_mek_diyarbakir41');
filesToOmit.push('rojda_kak_karsilasmalar_mekan_ozne_diyarbakir14');
filesToOmit.push('rojda_uak_mek_kars_siy_diyarbakir17');
filesToOmit.push('silan_kak_karsilasmalar_diyarbakir44');
filesToOmit.push('silan_uak_mekan_kars_siyas_diyarbakir47');

//Mugla
filesToOmit.push('buket_kak_iliskiler_siya_egi_mugla2');
filesToOmit.push('buket_uak_kars_egit_siya_mugla5');
filesToOmit.push('caner_kak_ekonomile_zam_mek_mugla50');
filesToOmit.push('caner_uak_eko_ozn_zam_mugla53');
filesToOmit.push('cenk_kak_mekan_ilis_ozne_mugla8');
filesToOmit.push('gozde_kak_ekonomiler_mugla26');
filesToOmit.push('lokman_ua_zam_ils_eko_mugla42');
filesToOmit.push('lokman_uak_eko_ilisk_egit_mugla47');
filesToOmit.push('lokman_uak_kars_ekon_zaman_mugla41');
filesToOmit.push('mugla32');
filesToOmit.push('okan_ka_zaman_eko_meka_mugla45');
filesToOmit.push('okan_kak_zaman_eko_meka_mugla44');


filesToChangeEng.push('esra_uak_ilisk_mekan_ozn_mugla17');
filesToChangeEng.push('hayrettin_kak_ekonomiler_mekan_zam_mugla56');
filesToChangeEng.push('ahmet_uak_siy_kars_ozn_diyarbakir5');
filesToChangeEng.push('gozde_uak_zaman_egitim_ozn_mugla29');
filesToChangeEng.push('sengul_uak_ilisk_siy_zam_berlin11');
filesToChangeEng.push('huseyin_uak_egit_kars_ozn_mugla35');

$(function(){


    _.each( filesToOmit,function(element, index, list){
        var theImg = $("img[src$='"+imageUrlBuilder(element)+"']");
        if( theImg.length > 0 ) {
            //var omit = theImg.parent('.egitim').hide();
            
            theImg.attr('src',"/assets/img/"+theImg.attr('class')+'.png');
            
            console.log(theImg.attr('class'));

            console.log(theImg.attr('src'));
        }
    });



    if( $('body').hasClass('lang_ku') ) {
        lang_prefix = '/ku/';
        currentLang = 'ku';
    }else if( $('body').hasClass('lang_en') ) {
        lang_prefix = '/en/';
        currentLang = 'en';
    }

    if( currentLang == 'en' ) {

        _.each( filesToChangeEng,function(element, index, list){
            var theImg = $("img[src$='"+imageUrlBuilder(element)+"']");
            if( theImg.length > 0 ) {
                //var omit = theImg.parent('.egitim').hide();
                
                theImg.attr('src',"/assets/img/"+theImg.attr('class')+'_en_hover.png');
                
                console.log(theImg.attr('class'));

                console.log(theImg.attr('src'));
            }
        });
    }

    //language links on left side
    var langClass = '.lang_'+currentLang;


    //makes the labels
    labelMaker( currentLang );



    $("a.karsilasmalar").attr("href",lang_prefix+"tag/karsilasmalar");
    $("a.zaman").attr("href",lang_prefix+"tag/zaman");
    $("a.ekonomi").attr("href",lang_prefix+"tag/ekonomiler");
    $("a.siyaset").attr("href",lang_prefix+"tag/siyaset");
    $("a.egitim").attr("href",lang_prefix+"tag/egitim");
    $("a.mekanlar").attr("href",lang_prefix+"tag/mekanlar");
    $("a.iliskiler").attr("href",lang_prefix+"tag/iliskiler");
    $("a.oznellik").attr("href",lang_prefix+"tag/oznellik");





    $('#news').load('/sozluk.html', function() {
        $('#news').vTicker({
            height:76,
            speed: 300,
            pause: 3000,
            animation: 'fade',
            mousePause: false,
            showItems: 3
        });
    });

    $('#tagkutusu').load(lang_prefix+'tagler.html', function() {
        $("div.karsilasmalartag").attr("onclick","location.href='"+lang_prefix+"tag/karsilasmalar'");
        $("div.zamantag").attr("onclick","location.href='"+lang_prefix+"tag/zaman'");
        $("div.ekonomitag").attr("onclick","location.href='"+lang_prefix+"tag/ekonomiler'");
        $("div.siyasettag").attr("onclick","location.href='"+lang_prefix+"tag/siyaset'");
        $("div.egitimtag").attr("onclick","location.href='"+lang_prefix+"tag/egitim'");
        $("div.mekanlartag").attr("onclick","location.href='"+lang_prefix+"tag/mekanlar'");
        $("div.iliskilertag").attr("onclick","location.href='"+lang_prefix+"tag/iliskiler'");
        $("div.oznelliktag").attr("onclick","location.href='"+lang_prefix+"tag/oznellik'");
    });

    $('#etkinlik').load(lang_prefix+'etkinlik.html',function(){
        $('#etkinlik').show();
    });

    var renkler=['zaman', 'iliskiler', 'siyaset', 'mekanlar','karsilasmalar', 'egitim', 'ekonomi', 'oznellik'];
    var rnd= Math.floor(Math.random()*8);







    $('.hoverlen').hover(function() {

        if( lang_prefix != '/' ) {
            //this.src=this.src.substr(0,this.src.length-4)+'_'+currentLang+'_hover.png';

            $(this).attr('src', this.src.substr(0,this.src.length-4)+'_'+currentLang+'_hover.png')            

        } else {
            //this.src=this.src.substr(0,this.src.length-4)+'_hover.png';
            $(this).attr('src', this.src.substr(0,this.src.length-4)+'_hover.png')            
        }
    }, function() {
        if( lang_prefix != '/' ) {

            this.src=this.src.substr(0,this.src.length-13)+'.png';
        } else {
            this.src=this.src.substr(0,this.src.length-10)+'.png';
        }

    });


    $('.ahmet,.ali,.rojda,.asim,.elif,.lutfu,.mehmet,.remzi,.silan,.caner,.hayrettin,.buket,.cenk,.esra,.ege,.gozde,.huseyin,.gul,.lokman,.okan,.sengul,.dilan').hover(function() {
        if (this.eskiImage == undefined) {
            this.eskiImage=this.src;
        }

        if( lang_prefix != '/' ) {
            
            this.src="/assets/img/hover/"+this.className+'_'+currentLang+'_hover.png';
        } else {

            this.src="/assets/img/"+this.className+'_hover.png';
        }

    }, function() {
        this.src=this.eskiImage;
    });


    $('img.esra').attr("onclick","location.href='"+lang_prefix+"transcripts/esra'");
    $('img.ahmet').attr("onclick","location.href='"+lang_prefix+"transcripts/ahmet'");
    $('img.ali').attr("onclick","location.href='"+lang_prefix+"transcripts/ali'");
    $('img.rojda').attr("onclick","location.href='"+lang_prefix+"transcripts/rojda'");
    $('img.asim').attr("onclick","location.href='"+lang_prefix+"transcripts/asim'");
    $('img.elif').attr("onclick","location.href='"+lang_prefix+"transcripts/elif'");
    $('img.lutfu').attr("onclick","location.href='"+lang_prefix+"transcripts/lutfu'");
    $('img.mehmet').attr("onclick","location.href='"+lang_prefix+"transcripts/mehmet'");
    $('img.remzi').attr("onclick","location.href='"+lang_prefix+"transcripts/remzi'");
    $('img.silan').attr("onclick","location.href='"+lang_prefix+"transcripts/silan'");
    $('img.caner').attr("onclick","location.href='"+lang_prefix+"transcripts/caner'");
    $('img.hayrettin').attr("onclick","location.href='"+lang_prefix+"transcripts/hayrettin'");
    $('img.buket').attr("onclick","location.href='"+lang_prefix+"transcripts/buket'");
    $('img.cenk').attr("onclick","location.href='"+lang_prefix+"transcripts/cenk'");
    $('img.ege').attr("onclick","location.href='"+lang_prefix+"transcripts/ege'");
    $('img.gozde').attr("onclick","location.href='"+lang_prefix+"transcripts/gozde'");
    $('img.huseyin').attr("onclick","location.href='"+lang_prefix+"transcripts/huseyin'");
    $('img.gul').attr("onclick","location.href='"+lang_prefix+"transcripts/gul'");
    $('img.lokman').attr("onclick","location.href='"+lang_prefix+"transcripts/lokman'");
    $('img.okan').attr("onclick","location.href='"+lang_prefix+"transcripts/okan'");
    $('img.sengul').attr("onclick","location.href='"+lang_prefix+"transcripts/sengul'");
    $('img.dilan').attr("onclick","location.href='"+lang_prefix+"transcripts/dilan'");


    //Change film url
    $('#arsiv').parent('a').attr('href',function(){
        var filmUrl = '/film.html';
        if( currentLang == 'ku' ) {
            filmUrl = '/film-kurdi.html';
        }
        if( currentLang == 'en' ) {
            filmUrl = '/film-english.html';
        }

            return filmUrl;
    });





});